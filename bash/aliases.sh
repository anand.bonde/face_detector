alias ctr='cd /playground/face_detector/python/src/ && python generate_training_set.py && python generate_model.py && python face_recognizer.py'
alias tr='cd /playground/face_detector/python/src/ && python generate_model.py && python face_recognizer.py'
alias fr='cd /playground/face_detector/python/src/ && python face_recognizer.py'
alias clean_fd='rm -rf cd /playround/face_detector/python/models/* cd ~/git/face_detector/python/training_data/*'
