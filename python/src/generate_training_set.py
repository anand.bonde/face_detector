"""
Generates the training data set from live camera feed
"""
import cv2
import os
from configparser import ConfigParser
from data_access import write_customer_info


config = ConfigParser()
config.read('config.ini')

training_data_dir = config.get('main_config', 'training_data_dir')
haar_cascade_dir = config.get('main_config', 'haarcascade_dir')
haar_cascade_file = config.get('main_config', 'haarcascade_file')
max_samples_per_user = config.getint('main_config', 'max_samples_per_user')
sampling_interval_ms = config.getint('main_config', 'sampling_interval_ms')
# applies to all sides
bounding_box_buffer = config.getint('main_config', 'bounding_box_buffer')


# load the haar cascade
faceDetect = cv2.CascadeClassifier(os.path.join(haar_cascade_dir,
                                                haar_cascade_file))

# capture the video stream
camera = cv2.VideoCapture(0)

# todo: must be an int
customer_name = input('Please enter the name?: ')
customer_id = write_customer_info(0, customer_name)
print('Auto generated id is: ' + str(customer_id))

i = 0

# capture the camera stream infinitely
#  1. convert to gray scale img
#  2. find faces in gray scale img
#  3. mark faces in color img
while i < max_samples_per_user:
    ret, img = camera.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = faceDetect.detectMultiScale(gray, 1.2)

    for face in faces:
        (x, y, width, height) = face

        # save current sample (only the detected face area),
        # and in gray scale only
        i = i + 1
        file_name = str(customer_id) + '.' + str(i) + '.pgm'
        file_path = os.path.join(training_data_dir, file_name)

        cv2.imwrite(file_path,
                    gray[y - bounding_box_buffer: y + height + bounding_box_buffer,
                         x - bounding_box_buffer: x + width + bounding_box_buffer],
                    [1])

        # draw the rectangle
        cv2.rectangle(
            img,
            (x - bounding_box_buffer, y - bounding_box_buffer),
            (x + width + bounding_box_buffer, y + height + bounding_box_buffer),
            (0, 255, 0),
            2)

        # pause for 100 ms. before taking the next shot
        cv2.waitKey(sampling_interval_ms)

    cv2.imshow("Face(s)", img)

    if cv2.waitKey(1) == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()
