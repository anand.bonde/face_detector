"""
Detects faces from the camera stream
"""
import os
from configparser import ConfigParser
import cv2
from data_access import read_all_customer_info

config = ConfigParser()
config.read('config.ini')

training_data_dir = config.get('main_config', 'training_data_dir')
haar_cascade_dir = config.get('main_config', 'haarcascade_dir')
haar_cascade_file = config.get('main_config', 'haarcascade_file')
models_dir = config.get('main_config', 'models_dir')
model_file = config.get('main_config', 'model_file')
recognition_interval_ms = config.getint(
    'main_config', 'recognition_interval_ms')
confidence_threshold = config.getint('main_config', 'confidence_threshold')
# applies to all sides
bounding_box_buffer = config.getint('main_config', 'bounding_box_buffer')


# load the haar cascade
faceDetect = cv2.CascadeClassifier(os.path.join(haar_cascade_dir,
                                                haar_cascade_file))

customers = read_all_customer_info()
print(customers)

# capture the video stream
cam = cv2.VideoCapture(0)

# create a recognizer (LBPH is a proven technique)
recognizer = cv2.face.LBPHFaceRecognizer_create()

recognizer.read(os.path.join(models_dir, model_file))

customer_id = 0

# capture the camera stream infinitely
#  1. convert to gray scale img
#  2. find faces in gray scale img
#  3. mark faces in color img
while True:
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = faceDetect.detectMultiScale(gray, 1.3, 1)

    for face in faces:
        (x, y, width, height) = face

        # use the model to generate a prediction
        customer_id, confidence = recognizer.predict(
            gray[y - bounding_box_buffer: y + height + bounding_box_buffer,
                 x - bounding_box_buffer: x + width + bounding_box_buffer])

        color = (0, 0, 255)
        if confidence < confidence_threshold:
            color = (0, 255, 0)

        # mark the face boundaries on the live feed
        cv2.rectangle(
            img,
            (x - bounding_box_buffer, y - bounding_box_buffer),
            (x + width + bounding_box_buffer, y + height + bounding_box_buffer),
            color,
            4)
        # annotate the live feed
        cv2.putText(
            img,
            'Id: ' + str(customer_id),
            (x - bounding_box_buffer, y + height + bounding_box_buffer + 30),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.7,
            color)
        cv2.putText(
            img,
            'Name: ' + customers[customer_id],
            (x - bounding_box_buffer, y + height + bounding_box_buffer + 60),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.7,
            color)
        cv2.putText(
            img,
            'Confidence: ' + str(round(confidence, 2)),
            (x - bounding_box_buffer, y + height + bounding_box_buffer + 90),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.7,
            color)

    cv2.imshow("Face(s)", img)
    if cv2.waitKey(recognition_interval_ms) == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()
