"""
Generates the model based on the training data set
"""
import os
import cv2
import numpy as np
from PIL import Image

from configparser import ConfigParser

config = ConfigParser()
config.read('config.ini')

training_data_dir = config.get('main_config', 'training_data_dir')
models_dir = config.get('main_config', 'models_dir')
model_file = config.get('main_config', 'model_file')


def get_images_and_ids(training_dir):
    """Gets all images and creates paths"""
    paths = get_file_paths(training_dir)

    customer_images = []
    customer_ids = []

    # ensure that all images are converted to gray scale
    # this may be redundant, as samples are gray scale already
    for path in paths:
        img = Image.open(path).convert('L')
        # cv2 only works with numpy arrays
        img_as_np = np.array(img, 'uint8')
        file_name = os.path.split(path)[-1]
        customer_id = int(file_name.split('.')[0])
        customer_images.append(img_as_np)
        customer_ids.append(customer_id)

    return np.array(customer_ids), customer_images


def get_file_paths(training_dir):
    paths = []
    files = os.listdir(training_dir)
    for f in files:
        path = os.path.join(training_dir, f)
        paths.append(path)
    return paths


ids, faces = get_images_and_ids(training_data_dir)

# create a recognizer (LBPH is a proven technique)
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.train(faces, ids)
recognizer.save(os.path.join(models_dir, model_file))

cv2.destroyAllWindows()
