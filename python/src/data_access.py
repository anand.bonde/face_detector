"""
Access to sqlite database
"""
from configparser import ConfigParser
import sqlite3

config = ConfigParser()
config.read('config.ini')
sqlite_file = config.get('main_config', 'sqlite_file')


def read_all_customer_info():
    """Reads all the customers' info. from the sqlite table and creates a
    dictionary based on the primary keys"""
    customers = {}
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    for row in c.execute('SELECT * FROM customers'):
        customer_id, customer_name = row
        customers[customer_id] = customer_name
    conn.close()
    return customers


def write_customer_info(customer_id, name):
    """ Writes the new customer info. to the sqlite table and returns the
    primary key of the inserted entity """
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    c.execute('INSERT INTO customers (name) VALUES (?)', (name,))
    print(c.lastrowid)
    conn.commit()
    conn.close()
    return c.lastrowid


def cleanup_customer_info():
    """Deletes all customer info. from the sqlite table"""
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    c.execute('DELETE FROM customers')
    conn.commit()
    conn.close()


if __name__ == '__main__':
    cleanup_customer_info()